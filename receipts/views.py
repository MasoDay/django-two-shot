from django.shortcuts import redirect, render, get_object_or_404
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_object": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def categories_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": category}
    return render(request, "categories/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("categories_list")
    else:
        form = CategoryForm()
        context = {
            "form": form,
        }
        return render(request, "categories/create.html", context)


@login_required
def accounts_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"accounts_list": account}
    return render(request, "accounts/list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts_list")
    else:
        form = AccountForm()
        context = {
            "form": form,
        }
        return render(request, "accounts/create.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)

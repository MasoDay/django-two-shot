from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    create_category,
    categories_list,
    create_account,
    accounts_list,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("categories/", categories_list, name="categories_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("accounts/", accounts_list, name="accounts_list"),
]
